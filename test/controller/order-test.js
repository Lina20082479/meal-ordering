let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../bin/www');
let expect = chai.expect;

chai.use(chaiHttp);
let _ = require('lodash' );
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6IjEyMzQ1NkBnbWFpbC5pZSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE1NDExODg1MjJ9.T3OvguFJAF4IJMomNQ2p6ibp7n3O06W14GYSptNNe4w';

describe('Order', function (){
    describe('when it has no permission', function(){
        it('should return a message', function(done) {
            chai.request(server)
              .get('/orders')
              .end((err, res) => {
                  expect(res).to.have.status(200);
                  expect(res.body).to.equal("Token not provided");
                  done();
              });
        });
    });
    describe('when it has permission', function(){
        describe('GET /orders',  () => {
            it('should return all the dishes in an array', function(done) {
                chai.request(server)
                .get('/orders')
                .set('Authorization', token)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body.length).to.equal(0);;
                    done();
                });
            }); 
        }); 
        describe('GET /orders/:id',  () => {
    
        });
        describe('POST /orders/',  () => {
    
        });
        describe('DELETE /orders/:id',  () => {
    
        });
        describe('PUT/orders/:id',  () => {
    
        });
    });
});
