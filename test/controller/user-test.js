let Users = require('../../models/user');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../bin/www');
let expect = chai.expect;

chai.use(chaiHttp);
let _ = require('lodash' );
chai.use(require('chai-things'));

describe('User', function (){
    beforeEach(function(done){  
        var users = new Users({
            id: 1,
            email: "12345@gmail.ie",
            password: "12345"
            });
        users.save(function(err){
                done();
        });
    });
    describe('GET /users',  () => {
        it('should return all users in an array', function(done) {
            chai.request(server)
              .get('/users')
              .end(function(err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.be.a('array');
                expect(res.body.length).to.equal(1);
                let result = _.map(res.body, (user) => {
                    return { id: user.id,
                        email: user.email } 
                    });
               expect(result).to.include[ { id: 1, email:'12345@gmail.ie' } ];
               done();
            });
        });
    });
    describe('GET /users/:id', function () {
        it('should return the user', function(done) {
           chai.request(server)
              .get('/users/1')
              .end(function(err, res) {
                  expect(res).to.have.status(200);
                  let result = _.map(res.body, (user) => {
                    return { id: user.id,
                        email: user.email } 
                    });
                    expect(result).to.include[ { id: 1, email:'12345@gmail.ie' } ];
                  done();
              });
        });
    });
    describe('POST /users', function () {
        it('should add one user', function(done) {
          let user = { 
               email:'12345678@gmail.ie',
               password:'12345678'
           };
          chai.request(server)
            .post('/users')
            .send(user)
            .end(function(err, res) {
              expect(res).to.have.status(200);
              done();
          });
      });
      after(function  (done) {
        chai.request(server)
            .get('/users')
            .end(function(err, res) {
                let result = _.map(res.body, (user) => {
                    return { email: user.email ,
                        password:user.password} 
                    });
                  expect(result).to.include[ { email:'12345678@gmail.ie', password:'12345678'} ];
                  done();
    
            });
      }); 
    });
    describe.only('DELETE /users/:id', function () {
        it('should return true and the user deleted ', function(done) {
            chai.request(server)
            .delete('/dishes/1')
            .end(function(err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.equal(true); 
              done();
            }); 
        }); 
        after(function  (done) {
           chai.request(server)
               .get('/users')
               .end(function(err, res) {
                   expect(res).to.have.status(200);
                   let result = _.map(res.body, (user) => {
                       return { id: user.id,
                        email: user.email} 
                    });
                   expect(result).to.not.include( { id: 1, email:'12345@gmail.ie' } );
                   done();
                });
           });
        });
        describe('PUT /users/:id', function () {
            it('should update the user information', function(done) {
               chai.request(server)
                  .put('/users/1')
                  .end(function(err, res) {
                      expect(res).to.have.status(200);
                      let result = res.body.data ;
                      expect(result).to.include( { id: 1, password: 1234  } );
                      done();
                  });
            });
      });
      afterEach(function(done){
        Users.collection.drop();
       done();
   });
});