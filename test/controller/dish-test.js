let Dishes = require('../../models/dish');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../bin/www');
let expect = chai.expect;

chai.use(chaiHttp);
let _ = require('lodash' );
chai.use(require('chai-things'));

describe('Dish', function (){
    beforeEach(function(done){  
        var dishes = new Dishes({
            name:"chicken & mushroom soup", 
            category : "soup", 
            description : "chicken breast,mushroom,egg,garlic,onions",
            price: 8
        });
        dishes.save(function(err){
                done();
        });
    });
    describe('GET /dishes',  () => {
        it('should return all the dishes in an array', function(done) {
            chai.request(server)
              .get('/dishes')
              .end(function(err, res) {
                expect(res).to.have.status(200);
                expect(res.body).to.be.a('array');
                expect(res.body.length).to.equal(1);

                let result = _.map(res.body, (dish) => {
                    return { name: dish.name,
                        price: dish.price } 
                    });
                expect(result).to.include( { name:"chicken & mushroom soup", price: 8 } );
                done();
            });
        });
    });
    describe('GET /dishes/:id', function () {
        it('should return the dish', function(done) {
            chai.request(server)
              .get('/dishes')
              .end(function(err, res) {
                const dishId = res.body[0]._id;
                chai.request(server)
                .get('/dishes/' + dishId)
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    expect(res.body.name).to.eql('chicken & mushroom soup');
                    done();
                });

              });
      
        });
    });
    describe('POST /dishes', function () {
        it('should add one dish', function(done) {
          let dish = { 
               name: "Crispy Crab Meat Roll" , 
               category:"starter",
               description: "spicy imitation crab paired with fresh avocado and cucumber", 
               price: 6
          };
          chai.request(server)
            .post('/dishes')
            .send(dish)
            .end(function(err, res) {
              expect(res).to.have.status(200);
              done();
          });
      });
      after(function  (done) {
        chai.request(server)
            .get('/dishes')
            .end(function(err, res) {
                let result = _.map(res.body, (dish) => {
                    return { name: dish.name,
                        price: dish.price } 
                    });
                  expect(result).to.include[{ name:"Crispy Crab Meat Roll", price: 6 } ];
                  done();
            });
    }); 
    });
    describe('DELETE /dishes/:id', function () {
        it('should return true and the dish deleted ', function(done) {
            chai.request(server)
            .get('/dishes')
            .end(function(err, res) {
                const dishId = res.body[0]._id;
                chai.request(server)
                    .delete('/dishes/'+dishId)
                    .end(function(err, res) {
                    expect(res).to.have.status(200);
                    expect(res.body).to.equal(true); 
                    done();
                });
            }) 
        }); 
       after(function  (done) {
           chai.request(server)
               .get('/dishes')
               .end(function(err, res) {
                   expect(res).to.have.status(200);
                   let result = _.map(res.body, (dish) => {
                       return { name: dish.name,
                        price: dish.price } 
                    });
                   expect(result).to.not.include( {name:"chicken & mushroom soup", price: 8 } );
                   done();
            });
        }); 
    });
    describe('PUT /dishes/:id', function () {
        it('should update the dish', function(done) {
            let dish = { 
                price: 18
           };
           chai.request(server)
            .get('/dishes')
            .end(function(err, res){
                const dishId = res.body[0]._id;
                chai.request(server)
                .put('/dishes/' + dishId)
                .send(dish)
                .end(function(err, res) {
                    expect(res).to.have.status(200);
                    expect(res.body).to.include( { name:"chicken & mushroom soup",  price: 18 } );
                    done();
                });
            })
   
        });

        it('should not update the dish', function(done) {
            let dish = { 
                price: 19
           };
           chai.request(server)
           .put('/dishes/wrong_id')
           .send(dish)
           .end(function(err, res) {
               expect(res).to.have.status(404);
               done();
           });
       });
    });
    describe('search',function(){
        it('should return the dish when it has key word in dish name  ', function(done) { 
            chai.request(server)
            .get('/dish-search/name/so')
            .end(function(err, res) {
                expect(res).to.have.status(200);
                let result = _.map(res.body, (dish) => {
                    return { name: dish.name,
                     price: dish.price } 
                 });
                expect(result).to.include[ {name:"chicken & mushroom soup", price: 8 } ];
                done();
             });
        });
    });
    afterEach(function(done){
       Dishes.collection.drop();
       done();
   });
});


  
   
  
